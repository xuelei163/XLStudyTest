

Pod::Spec.new do |s|

  s.name         = "StudyTest"
  s.version      = "0.0.1"
  s.summary      = "StudyTest."

  s.description  = <<-DESC
           测试私有库
                   DESC

  s.homepage     = "https://gitee.com/xuelei163/XLStudyTest"

  s.license      = "MIT"

  s.author             = { "xuelei" => "" }

  s.platform     = :ios, "9.0"

  s.source       = { :git => "https://gitee.com/xuelei163/XLStudyTest.git", :tag => "#{s.version}" }


 s.source_files  = "StudyDemo", "StudyDemo/**/*.{h,m}"
 s.framework = "Foundation"

end
